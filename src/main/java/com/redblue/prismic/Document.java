package com.redblue.prismic;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Document {
	
	private Map<String, JsonNode> attrs = new HashMap<String, JsonNode>();
	
	public JsonNode getAttribute(String key) {
		return attrs.get(key);
	}
	
	public Map<String, JsonNode> getAttributes() {
		return attrs;
	}

	public void setAttribute(String key, JsonNode value) {
		attrs.put(key, value);
	}
	
	public boolean isValid() {
		return attrs.containsKey("id");
	}
	
	public JsonNode toJson() {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.createObjectNode();
		
		((ObjectNode) node).setAll(attrs);
		
		return node;
	}
}
