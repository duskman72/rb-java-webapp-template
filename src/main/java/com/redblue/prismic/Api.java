package com.redblue.prismic;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Api {
	private static String urlString;
	private String master;
	private static Map<String, Document> documents = new HashMap<String, Document>();
	
	
	public static Api get(String urlString) throws Exception {
		Api.urlString = urlString;
		Api api = new Api();
		return api;
	}
	
	public Document getByID(String id) throws Exception {
		Document doc = Api.documents.containsKey(id) ? Api.documents.get(id) : new Document();
		return doc;
	}
	
	public Document[] getDocuments() {
		Document docs[] = new Document[Api.documents.size()];
		int index = 0;
		for(Document doc : Api.documents.values()) {
			docs[index] = doc;
			index++;
		}
		
		return docs;
	}
	
	private void mapDocuments(JsonNode parent) {
		if(parent.isArray()) {
			ArrayNode node = (ArrayNode) parent;
			for(int index = 0; index < node.size(); index++) {
				JsonNode child = parent.get(index);
				mapDocuments(child);
			}
		}
		
		if (parent.isObject()) {
			Iterator<String> fNames = parent.fieldNames();
			while(fNames.hasNext()) {
				String fieldName = fNames.next();
				JsonNode child = parent.get(fieldName);
				if (child.isArray()) {
					mapDocuments(child);
				} else {
					if (child.isObject() && fieldName.toLowerCase().equals("page")) {
						String id = child.get("id").asText();
						Map<String, JsonNode> attrs = documents.get(id).getAttributes();
						JsonNode data = attrs.containsKey("data") ? attrs.get("data") : null;
						
						if (data != null) {
							attrs.remove("data");
						}
						((ObjectNode)child).setAll(attrs);
						
						if (data != null) {
							Iterator<String> dataFieldNames = data.fieldNames();
							while(dataFieldNames.hasNext()) {
								String dataFieldName = dataFieldNames.next();
								((ObjectNode)child).set(dataFieldName, data.get(dataFieldName));
							}
						}
						
						// TODO remove page attribute and merge to parent
					}
					
					mapDocuments(child);
				}
			}
		}
	}
	
	private Api() throws Exception {
		try {
			URL url = new URL(Api.urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.addRequestProperty("X-Requeted-With", "XMLHttpRequest");
			connection.addRequestProperty("Accept", "application/json");
			
			connection.setUseCaches(false);
		    connection.setDoOutput(true);
		    
		    InputStream is = connection.getInputStream();
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		    StringBuffer response = new StringBuffer(); // or StringBuffer if Java version 5+
		    String line;
		    while ((line = rd.readLine()) != null) {
		      response.append(line);
		    }
		    rd.close();
		    
		    JSONArray refs = (new JSONObject(response.toString())).getJSONArray("refs");
		    
			for(int i = 0; i < refs.length(); i++) {
				if (refs.getJSONObject(i).getBoolean("isMasterRef")) {
					this.master = refs.getJSONObject(i).getString("ref");
					break;
				}
			}
			
			URL search_url = new URL(Api.urlString.concat("/documents/search?ref=".concat(this.master)));
			HttpURLConnection search_connection = (HttpURLConnection) search_url.openConnection();
			search_connection.setRequestMethod("GET");
			search_connection.addRequestProperty("X-Requeted-With", "XMLHttpRequest");
			search_connection.addRequestProperty("Accept", "application/json");
			
			search_connection.setUseCaches(false);
			search_connection.setDoOutput(true);
		    
		    InputStream search_is = search_connection.getInputStream();
		    BufferedReader search_rd = new BufferedReader(new InputStreamReader(search_is));
		    StringBuffer search_response = new StringBuffer(); // or StringBuffer if Java version 5+
		    String search_line;
		    while ((search_line = search_rd.readLine()) != null) {
		      search_response.append(search_line);
		    }
		    search_rd.close();
		    search_connection.disconnect();
		    
		    ObjectMapper objectMapper = new ObjectMapper();
		    JsonNode jNode = objectMapper.readTree(search_response.toString()).at("/results");
		   
		    for(int index = 0; index < jNode.size(); index++) {
		    	JsonNode child = jNode.get(index);
		    	Document doc = new Document();
		    	
		    	Iterator<Map.Entry<String, JsonNode>> fields = child.fields();
		    	while(fields.hasNext()) {
		    		Map.Entry<String, JsonNode> field = fields.next();
		    		if (field.getKey().length() != 0)
		    			doc.setAttribute(field.getKey(), field.getValue());
		    	}
		    	
		    	if (doc.isValid()) {
		    		String id = doc.getAttribute("id").asText();
			    	Api.documents.put(id,  doc);
		    	}
		    }
		    
		    for(Map.Entry<String, Document> entry: Api.documents.entrySet()) {
		    	Document doc = entry.getValue();
		    	mapDocuments(doc.toJson());
		    }
		} catch (Exception e) {
			throw e;
		}
		
	}
}
