package com.redblue.dev;

import com.redblue.graphql.GraphQLService;
import com.redblue.graphql.PrismicRepository;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;

import com.coxautodev.graphql.tools.SchemaParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.redblue.prismic.Api;
import com.redblue.prismic.Document;

@Controller
public class IndexController implements ErrorController {
	
	@GetMapping("/")
	public String index() {
        return "frontend";
    }
	
	@RequestMapping(value = "/api/prismic/document", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> document(HttpServletRequest request) throws Exception {
		Api api = Api.get("https://mms-16.prismic.io/api/v2");
		String id = request.getParameter("id");
		String path = request.getParameter("path");
		path = path == null || path.length() == 0 ? "/" : path;
		Document doc = api.getByID(id);
		
		if (doc.isValid()) {
			if (path != null && path.equals("/"))
				return ResponseEntity.ok(doc.getAttributes());
			else {
				if(!path.startsWith("/"))
					return ResponseEntity.status(500).body("{\"error\":\"true\",\"message\":\"path must start with '/'\",\"status\":500}");
				
				JsonNode node = doc.toJson();
				return ResponseEntity.ok(node.at(path));
			}
		}
		
		return ResponseEntity.status(500).body("{\"error\":\"true\",\"message\":\"unknown document '"+id+"'\",\"status\":500}");
	}

	@Bean
    public GraphQL graphQL() {
		GraphQLService graphqlService = new GraphQLService(new PrismicRepository());

		GraphQLSchema schema = SchemaParser.newParser()
		.file("schema.graphqls")
		.resolvers(graphqlService)
		.build()
		.makeExecutableSchema();

		return new GraphQL.Builder(schema).build();
	}
	
	@Autowired
    private GraphQL graphQL;

    @RequestMapping(value = "/graphql", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ExecutionResult execute(@RequestBody Map<String, Object> request) {
        return graphQL.execute(ExecutionInput.newExecutionInput()
                .query((String) request.get("query"))
				.operationName((String) request.get("operationName"))
				.variables((Map<String, Object>) request.get("variables"))
                .build());
    }

	@RequestMapping(value = "/error", produces = "application/json")
	public ResponseEntity<?> error(HttpServletRequest request) {
		JSONObject obj = new JSONObject();
		obj.put("error", true);
		obj.put("status", request.getAttribute("javax.servlet.error.status_code"));
		obj.put("message", request.getAttribute("javax.servlet.error.message"));
		return ResponseEntity.ok(obj.toString());
	}
	
	@Override
    public String getErrorPath() {
        return "/error";
    }
}
