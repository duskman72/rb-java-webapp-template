package com.redblue.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:application.properties")

@SpringBootApplication
public class Application extends SpringBootServletInitializer {
	
	@Override   
	protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(final String[] args) {
    	System.setProperty("https.proxyHost", "bluecoat.media-saturn.com");
    	System.setProperty("https.proxyPort", "80");
        SpringApplication.run(Application.class, args);
    }

}