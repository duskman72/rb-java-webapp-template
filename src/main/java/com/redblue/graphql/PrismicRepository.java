package com.redblue.graphql;

import org.springframework.stereotype.Repository;

@Repository
public class PrismicRepository {
    public PrismicRepository() {

    }

    public PrismicPage byId(String page_id) {
        return new PrismicPage(page_id);
    }
}