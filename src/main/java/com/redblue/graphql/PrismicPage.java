package com.redblue.graphql;

public class PrismicPage {
    private String page_id;
    public PrismicPage(String page_id) {
        this.page_id = page_id;
    }

    public String getName() {
        return "This is my name";
    }

    public String getPage_id() {
        return this.page_id;
    }

    public Integer getId() {
        return 1;
    }
}
