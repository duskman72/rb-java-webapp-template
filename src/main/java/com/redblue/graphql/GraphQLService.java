package com.redblue.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import io.leangen.graphql.annotations.GraphQLQuery;

public class GraphQLService implements GraphQLQueryResolver {
    private PrismicRepository repo;

    public GraphQLService (PrismicRepository repo) {
        this.repo = repo;
    }

    @GraphQLQuery
    public PrismicPage getPage(String page_id) {
        return repo.byId(page_id);
    }
}